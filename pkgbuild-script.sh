#!/bin/sh

# list of jails we want to run builds for
JAILS="FreeBSD:13:amd64"
# list of portstrees
PORTS="quarterly latest"
# tuples of sets (see poudriere -z option) and portstrees to build
SETS_PORTS="servers:quarterly desktop:latest"

# build these fat ports first to avoid ressource starvation
BUILDFIRST="lang/rust lang/gcc devel/llvm www/node"

# paths
POUDRIERE="/usr/local/bin/poudriere"
GIT="/usr/local/bin/git"
BASEDIR="/usr/local/etc/poudriere.d"
PORTSDIR="/usr/local/poudriere/ports"

# first update ports trees; if something fails here we can exit early
for p in ${PORTS?"no portstree defined"}; do
	# poudriere runs 'git pull --rebase' and messes up our manual rebases, so we use git directly to update the ports tree
	${GIT} -C ${PORTSDIR}/${p} pull || exit "error while updating ports tree ${p}"
done

# build loops - not my proudest work...
for j in ${JAILS?"no jail defined"}; do
	# update jail
	${POUDRIERE} jail -u -j ${j} || break "error while updating jail ${j}"
	for i in ${SETS_PORTS?"no sets+ports tuples defined"}; do
		s="$(echo $i | cut -d':' -f1)"
		p="$(echo $i | cut -d':' -f2)"
		# build ports with insanely high ressource requirements first
		for b in ${BUILDFIRST}; do
			${POUDRIERE} bulk -j ${j} -p ${p?"no portstree defined in tuple ${i}"} -z ${s?"no set defined in tuple ${i}"} ${b}
		done
		${POUDRIERE} bulk -j ${j} -p ${p?"no portstree defined in tuple ${i}"} -z ${s?"no set defined in tuple ${i}"} -f ${BASEDIR}/pkglist-${s} 
	done
done

# cleanup logs
#poudriere logclean 30 -y
